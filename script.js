var onClickSubcategory
(function() {

  var allCategories = {};
  var subCategories = {};

  main();

  function main() {
    getCategoriesFromServer().then(function(categories) {
      renderCategories('all-categories-dropdown', categories.splice(0, 5));
      var params = getUrlParams();
      if(params.categoryId) {
        showCategoryModal(params.categoryId);
      }
    });
  }

  function getCategoriesFromServer() {
    return new Promise(function(resolve, reject) {
      jQuery.ajax("https://priyankabarde.github.io/category_data.json")
        .done(resolve)
        .fail(reject);
    });
  }

  function renderCategories(elemId, categories) {
    var html = categories.reduce(function(catAgg, category) {
      category[0].subLength = category.subCategories.length;
      allCategories[category[0].id.toString()] = category[0];
      catAgg += '<div class="col dropdown-col">';
      catAgg += '<p class="dropdown-col-title"><a target="_blank" href="./?categoryId='+ category[0].id + '">' + category[0].name + '</a></p>';
      catAgg += '<ul class="dropdown-col-list">';
      catAgg += category.subCategories.reduce(function(subAgg, subCategory) {
        subCategories[subCategory.id] = subCategory;
        subAgg += '<li data-toggle="modal" data-target="#category-modal" data-subId="' + subCategory.id + '">' + subCategory.name + '</li>';
        return subAgg;
      }, '');
      catAgg += '</ul></div>'
      return catAgg
    }, '');

    document.getElementById(elemId).innerHTML = html;
  }


  function getUrlParams() {
    var paramsObj = {};
    var indexOfLimiter = window.location.href.indexOf('?') + 1;
    if (indexOfLimiter == 0) {
      return {};
    }
    var hashes = window.location.href.slice(indexOfLimiter).split('&');
    for (var i = 0; i < hashes.length; i++) {
      var hash = hashes[i].split('=');
      paramsObj[hash[0]] = hash[1];
    }
    return paramsObj;
  }

  function showCategoryModal(categoryId) {
    if(typeof allCategories[categoryId] === 'undefined') {
      return;
    }
    
    var category = allCategories[categoryId];
    document.getElementById('category-img').src = category.image_url;
    document.getElementById('category-type').innerHTML = 'category';
    document.getElementById('category-title').innerHTML = category.name;
    document.getElementById('category-text').innerHTML = 'There are ' + category.subLength + ' sub-categories and ' + category.offer_count + ' offers available for "' + category.name +'"';
    $('#category-modal').modal();
  }

  function renderSubcategory(id) {
    console.log(id, subCategories)
    if(typeof subCategories[id] === 'undefined') {
      return;
    }
    var sub = subCategories[id];
    console.log(sub);
    document.getElementById('category-img').src = 'http://pluspng.com/img-png/special-offer-png-filename-special-offer-png-315.png';
    document.getElementById('category-type').innerHTML = 'sub category';
    document.getElementById('category-title').innerHTML = sub.name;
    document.getElementById('category-text').innerHTML = 'There are ' + sub.offer_count + ' offers available for "' + sub.name +'"';
  }

  $('#category-modal').on('show.bs.modal', function (event) {
    console.log(event)
    if (typeof event.relatedTarget === 'undefined') {
      return;
    }
    renderSubcategory($(event.relatedTarget).attr('data-subId'));
  });
})();